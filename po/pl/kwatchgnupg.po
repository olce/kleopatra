# translation of kwatchgnupg.po to
# Krzysztof Lichota <lichota@mimuw.edu.pl>, 2004, 2005.
# Marta Rybczyńska <kde-i18n@rybczynska.net>, 2007, 2008.
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2011, 2015, 2016, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kwatchgnupg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-16 00:39+0000\n"
"PO-Revision-Date: 2019-08-03 07:30+0200\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 19.07.70\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Krzysztof Lichota, Łukasz Wojniłowicz"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "lichota@mimuw.edu.pl, lukasz.wojnilowicz@gmail.com"

#: aboutdata.cpp:26
msgid "Steffen Hansen"
msgstr "Steffen Hansen"

#: aboutdata.cpp:26
msgid "Original Author"
msgstr "Autor pierwszej wersji"

#: aboutdata.cpp:31
#, kde-format
msgid "KWatchGnuPG"
msgstr "KWatchGnuPG"

#: aboutdata.cpp:33
#, kde-format
msgid "GnuPG log viewer"
msgstr "Przeglądarka dziennika GnuPG"

#: aboutdata.cpp:35
#, kde-format
msgid "(c) 2004 Klarälvdalens Datakonsult AB\n"
msgstr "(c) 2004 Klarälvdalens Datakonsult AB\n"

#: kwatchgnupgconfig.cpp:55
#, kde-format
msgctxt "@title:window"
msgid "Configure KWatchGnuPG"
msgstr "Ustawienia KWatchGnuPG"

#: kwatchgnupgconfig.cpp:71
#, kde-format
msgid "WatchGnuPG"
msgstr "WatchGnuPG"

#: kwatchgnupgconfig.cpp:81
#, kde-format
msgid "&Executable:"
msgstr "Plik &wykonywalny:"

#: kwatchgnupgconfig.cpp:90
#, kde-format
msgid "&Socket:"
msgstr "&Gniazdo:"

#: kwatchgnupgconfig.cpp:99
#, kde-format
msgid "None"
msgstr "Brak"

#: kwatchgnupgconfig.cpp:100
#, kde-format
msgid "Basic"
msgstr "Podstawowy"

#: kwatchgnupgconfig.cpp:101
#, kde-format
msgid "Advanced"
msgstr "Zaawansowany"

#: kwatchgnupgconfig.cpp:102
#, kde-format
msgid "Expert"
msgstr "Ekspert"

#: kwatchgnupgconfig.cpp:103
#, kde-format
msgid "Guru"
msgstr "Guru"

#: kwatchgnupgconfig.cpp:104
#, kde-format
msgid "Default &log level:"
msgstr "Domyś&lny poziom dziennika:"

#: kwatchgnupgconfig.cpp:112
#, kde-format
msgid "Log Window"
msgstr "Okno dziennika"

#: kwatchgnupgconfig.cpp:124
#, kde-format
msgctxt "history size spinbox suffix"
msgid " line"
msgid_plural " lines"
msgstr[0] " wiersz"
msgstr[1] " wiersze"
msgstr[2] " wierszy"

#: kwatchgnupgconfig.cpp:125
#, kde-format
msgid "unlimited"
msgstr "bez ograniczeń"

#: kwatchgnupgconfig.cpp:126
#, kde-format
msgid "&History size:"
msgstr "Rozmiar &historii:"

#: kwatchgnupgconfig.cpp:130
#, kde-format
msgid "Set &Unlimited"
msgstr "&Ustaw bez ograniczeń"

#: kwatchgnupgconfig.cpp:137
#, kde-format
msgid "Enable &word wrapping"
msgstr "Włącz za&wijanie słów"

#: kwatchgnupgmainwin.cpp:77
#, kde-format
msgid "[%1] Log cleared"
msgstr "[%1] Wyczyszczono dziennik"

#: kwatchgnupgmainwin.cpp:84
#, kde-format
msgid "C&lear History"
msgstr "&Wyczyść historię"

#: kwatchgnupgmainwin.cpp:114
#, kde-format
msgid "[%1] Log stopped"
msgstr "[%1] Zatrzymano rejestrowanie"

#: kwatchgnupgmainwin.cpp:131
#, kde-format
msgid ""
"The watchgnupg logging process could not be started.\n"
"Please install watchgnupg somewhere in your $PATH.\n"
"This log window is unable to display any useful information."
msgstr ""
"Nie można uruchomić procesu rejestrowania watchgnupg.\n"
"Proszę zainstalować watchgnupg gdzieś w twojej ścieżce $PATH.\n"
"To okno dziennika nie jest w stanie wyświetlić jakiejkolwiek użytecznej "
"informacji."

#: kwatchgnupgmainwin.cpp:134
#, kde-format
msgid "[%1] Log started"
msgstr "[%1] Rozpoczęto dziennik"

#: kwatchgnupgmainwin.cpp:169
#, kde-format
msgid "There are no components available that support logging."
msgstr "Nie ma dostępnych składników obsługujących rejestrowanie."

#: kwatchgnupgmainwin.cpp:176
#, kde-format
msgid ""
"The watchgnupg logging process died.\n"
"Do you want to try to restart it?"
msgstr ""
"Rejestrowanie do dziennika watchgnupg przestało działać.\n"
"Czy chcesz spróbować rozpocząć je ponownie?"

#: kwatchgnupgmainwin.cpp:178
#, fuzzy, kde-format
#| msgid "Try Restart"
msgctxt "@action:button"
msgid "Try Restart"
msgstr "Spróbuj ponownie"

#: kwatchgnupgmainwin.cpp:179
#, fuzzy, kde-format
#| msgid "Do Not Try"
msgctxt "@action:button"
msgid "Do Not Try"
msgstr "Nie próbuj"

#: kwatchgnupgmainwin.cpp:181
#, kde-format
msgid "====== Restarting logging process ====="
msgstr "====== Rozpoczynanie rejestrowania na nowo ====="

#: kwatchgnupgmainwin.cpp:185
#, kde-format
msgid ""
"The watchgnupg logging process is not running.\n"
"This log window is unable to display any useful information."
msgstr ""
"Nie uruchomiono procesu rejestrowania watchgnupg.\n"
"To okno dziennika nie jest w stanie wyświetlić jakiejkolwiek użytecznej "
"informacji."

#: kwatchgnupgmainwin.cpp:220
#, kde-format
msgid "Save Log to File"
msgstr "Zapisz dziennik do pliku"

#: kwatchgnupgmainwin.cpp:228
#, kde-format
msgid "Could not save file %1: %2"
msgstr "Nie można zapisać pliku %1: %2"

#. i18n: ectx: Menu (file)
#: kwatchgnupgui.rc:4
#, kde-format
msgid "&File"
msgstr "&Plik"

#. i18n: ectx: ToolBar (mainToolBar)
#: kwatchgnupgui.rc:13
#, kde-format
msgid "Main Toolbar"
msgstr "Główny pasek narzędzi"

#: tray.cpp:30
#, kde-format
msgid "KWatchGnuPG Log Viewer"
msgstr "Przeglądarka dziennika KWatchGnuPG"

#~ msgid ""
#~ "The file named \"%1\" already exists. Are you sure you want to overwrite "
#~ "it?"
#~ msgstr "Plik \"%1\" już istnieje. Czy na pewno chcesz go zastąpić?"

#~ msgid "Overwrite File"
#~ msgstr "Zastąpienie pliku"
